package snhu.jukebox.playlist;

import snhu.student.playlists.*;

import java.util.ArrayList;
import java.util.List;

import AustenChaney_Playlist.AustenChaney_Playlist;

public class StudentList {

	public StudentList(){
	}

	public List<String> getStudentsNames() {
		ArrayList<String> studentNames = new ArrayList<String>();

		String StudentName1 = "TestStudent1Name";
		studentNames.add(StudentName1);

		String StudentName2 = "TestStudent2Name";
		studentNames.add(StudentName2);

		//Module 5 Code Assignment
		//Add your name to create a new student profile
		//Use template below and put your name in the areas of 'StudentName'
		
		String AustenChaney = "Austen Chaney";
		studentNames.add(AustenChaney);
		
		String RachelSickler = "Rachel Sickler";
		studentNames.add(RachelSickler);

		String RyleeThompson = "Rylee Thompson";
		studentNames.add(RyleeThompson);

		String JimLouis = "Jim Louis";
		studentNames.add(JimLouis);

		String TravisOwens = "Travis Owens";
		studentNames.add(TravisOwens);
		
		String FelixTudoran = "Felix Tudoran";
		studentNames.add(FelixTudoran);
		
		String DanGuerin = "Dan Guerin";
		studentNames.add(DanGuerin);
		
		String AlexHamilton = "Alex Hamilton";
		studentNames.add(AlexHamilton);

		String BenjaminWeaver = "Benjamin Weaver";
		studentNames.add(BenjaminWeaver);
		
		String StephenZaferopolos = "Stephen Zaferopolos";
		studentNames.add(StephenZaferopolos);

		String TylerBeckham = "Tyler Beckham";
		studentNames.add(TylerBeckham);
		
		return studentNames;
	}

	public Student GetStudentProfile(String student){
		Student emptyStudent = null;

		switch(student) {
		   case "TestStudent1_Playlist":
			   TestStudent1_Playlist testStudent1Playlist = new TestStudent1_Playlist();
			   Student TestStudent1 = new Student("TestStudent1", testStudent1Playlist.StudentPlaylist());
			   return TestStudent1;

		   case "TestStudent2_Playlist":
			   TestStudent2_Playlist testStudent2Playlist = new TestStudent2_Playlist();
			   Student TestStudent2 = new Student("TestStudent2", testStudent2Playlist.StudentPlaylist());
			   return TestStudent2;
			   
		   case "AustenChaney_Playlist":
			   AustenChaney_Playlist AustenChaneyPlaylist = new AustenChaney_Playlist();
			   Student AustenChaney = new Student("Austen Chaney", AustenChaneyPlaylist.StudentPlaylist());
			   return AustenChaney;

		   case "BenjaminWeaver_Playlist":
			   BenjaminWeaver_Playlist BenjaminWeaverPlaylist = new BenjaminWeaver_Playlist();
			   Student BenjaminWeaver = new Student("Benjamin Weaver", BenjaminWeaverPlaylist.StudentPlaylist());
			   return BenjaminWeaver;
			   
		   case "TylerBeckham_Playlist":
			   TylerBeckham_Playlist TylerBeckhamPlaylist = new TylerBeckham_Playlist();
			   Student TylerBeckham = new Student("Tyler Beckham", TylerBeckhamPlaylist.StudentPlaylist());
			   return TylerBeckham;

		   //Module 6 Code Assignment - Add your own case statement for your profile. Use the above case statements as a template.

		}
		return emptyStudent;
	}
}
