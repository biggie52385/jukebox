package snhu.student.playlists;

import java.util.ArrayList;
import java.util.LinkedList;

import music.artist.Arkana;
import music.artist.TheNotoriousBIG;
import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;

public class AustenChaney_Playlist {
	
	public LinkedList<PlayableSong> StudentPlaylist(){
		
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		ArrayList<Song> TheNotoriousBIGSongs = new ArrayList<Song>();
	    TheNotoriousBIG TheNotoriousBIG = new TheNotoriousBIG();
		
	    TheNotoriousBIGSongs = TheNotoriousBIG.getTheNotoriousBIGSongs();
		
		playlist.add(TheNotoriousBIGSongs.get(0));
		playlist.add(TheNotoriousBIGSongs.get(1));
		
		ArrayList<Song> ArkanaSongs = new ArrayList<Song>();
	
	    Arkana Arkana = new Arkana();
		
	    ArkanaSongs = Arkana.getArkanaSongs();
		
		playlist.add(ArkanaSongs.get(0));
		playlist.add(ArkanaSongs.get(1));
		playlist.add(ArkanaSongs.get(2));
		
	    return playlist;
	}

}
